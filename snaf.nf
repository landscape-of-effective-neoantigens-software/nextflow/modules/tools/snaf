process altanalyze {

  publishDir "${params.samps_out_dir}/altanalyze"
  label 'altanalyze_container'
  label 'altanalyze'

  input:
  path bams
  val parstr

// output:
//  tuple val(pat_name), val(prefix), val(dataset), path("*optitype_calls*"), emit: calls

  script:
  """
  /usr/src/app/AltAnalyze.sh identify bam ${task.cpus}
  """
}

process snaf {

  publishDir "${params.samps_out_dir/snaf"
  label "snaf_container"
  label "altanalyze"

  input:
  path counts
  path hla_alleles
  path refs

  script:
  """
  import os,sys
  import pandas as pd
  import numpy as np
  import anndata as ad
  import snaf

  # read in the splicing junction matrix
  df = pd.read_csv($counts,index_col=0,sep='\t')

  # database directory (where you extract the reference tarball file) and netMHCpan folder
  db_dir = '/user/ligk2e/download'
  netMHCpan_path = '/user/ligk2e/netMHCpan-4.1/netMHCpan'

  # demonstrate how to add additional control database, see below note for more
  tcga_ctrl_db = ad.read_h5ad(os.path.join(refs,'controls','tcga_matched_control_junction_count.h5ad'))
  gtex_skin_ctrl_db = ad.read_h5ad(os.path.join(refs,'controls','gtex_skin_count.h5ad'))
  add_control = {'tcga_control':tcga_ctrl_db,'gtex_skin':gtex_skin_ctrl_db}

  # initiate
  snaf.initialize(df=df,db_dir=db_dir,binding_method='netMHCpan',software_path=netMHCpan_path,add_control=add_control)

  jcmq = snaf.JunctionCountMatrixQuery(junction_count_matrix=df,cores=${task.cpus},add_control=add_control,outdir='result')

  sample_to_hla = pd.read_csv(${hla_alleles},sep='\t',index_col=0)['hla'].to_dict()
  hlas = [hla_string.split(',') for hla_string in df.columns.map(sample_to_hla)]

  jcmq.run(hlas=hlas,outdir='./result')
  """
}
